/**
 * Automatically generated file. DO NOT MODIFY
 */
package rp3.mkfmobile;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "rp3.mkfmobile";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 3;
  public static final String VERSION_NAME = "0.0.3";
}
