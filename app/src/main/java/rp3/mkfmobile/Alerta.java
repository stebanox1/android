package rp3.mkfmobile;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import dmax.dialog.SpotsDialog;

/**
 * Created by gandrade on 16/05/16.
 */
@SuppressLint("InflateParams")
public class Alerta {
    private Context context;
    private LayoutInflater layout;
  private AlertaAdapter adaptador_alerta;

    public Alerta(Context context, AlertaAdapter adapter) {
        super();
        this.context = context;
        this.adaptador_alerta = adapter;
        this.layout = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

        public static SpotsDialog mostrarProgreso(Context _context) {
          try {
            if (_context != null) {
              SpotsDialog dlg_progreso = new SpotsDialog(_context, "Procesando...", R.style.Custom);
              if (dlg_progreso != null) {
                // dlg_progreso.setMessage(_context.getString(R.string.procesando));
                dlg_progreso.setCancelable(true);
              }
              return dlg_progreso;
            }
          } catch (Exception e) {
            e.printStackTrace();
          }
          return null;
        }
}
