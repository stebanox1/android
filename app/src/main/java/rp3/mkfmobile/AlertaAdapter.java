package rp3.mkfmobile;

import android.app.Dialog;

/**
 * Created by gandrade on 16/05/16.
 */
public interface AlertaAdapter {
    void _alertaAceptar();

    void _alertaOTP(String codigo, Dialog dialog);
}
