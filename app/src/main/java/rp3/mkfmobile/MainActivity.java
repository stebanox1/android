/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package rp3.mkfmobile;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.format.Formatter;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.embedding.engine.FlutterEngineCache;
import io.flutter.embedding.engine.dart.DartExecutor;

public class MainActivity extends CordovaActivity implements PagosAdapter
{
  private ProgressDialog dialog;
  String IdTransaccion="";
  FlutterEngine flutterEngine;
  WebView webView;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // enable Cordova apps to be started in the background
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.getBoolean("cdvStartInBackground", false)) {
            moveTaskToBack(true);
        }

      // Set by <content src="index.html" /> in config.xml
        loadUrl(launchUrl);


      webView = (WebView) appView.getEngine().getView();
//      webView.addJavascriptInterface(this.getJavaScriptHandler(), "Android");
    }

  public String getdata(String data){
    JSONObject json = new JSONObject();
    JSONObject jsoncliente = new JSONObject();
    JSONObject jsonproducto = new JSONObject();
    JSONObject jsonitem = new JSONObject();
    JSONArray jsonitems = new JSONArray();
    try {

      JSONObject jObj = new JSONObject(data);
      JSONObject cliente = new JSONObject(!jObj.isNull("cliente") ? jObj.getString("cliente"):"");
      JSONArray items = new JSONArray(!jObj.isNull("detalles") ? jObj.getString("detalles"):"");
      System.out.println("items "+ items);

      String IDCOMERCIO = !jObj.isNull("IDCOMERCIO") ? jObj.getString("IDCOMERCIO"):"00";
      System.out.println("IDCOMERCIO "+ IDCOMERCIO);

      String TOTAL = !jObj.isNull("efectivoval") ? jObj.getString("efectivoval"):jObj.getString("Total");
      System.out.println("TOTAL "+ TOTAL);

      String SubtotalSinImpuesto = !jObj.isNull("SubtotalSinImpuesto") ? jObj.getString("SubtotalSinImpuesto"):"00";
      System.out.println("SubtotalSinImpuesto "+ SubtotalSinImpuesto);

      String subtotalsiniva = !jObj.isNull("subtotalsiniva") ? jObj.getString("subtotalsiniva"):"00";
      System.out.println("subtotalsiniva "+ subtotalsiniva);

      String subtotaliva = !jObj.isNull("subtotaliva") ? jObj.getString("subtotaliva"):"00";
      System.out.println("subtotaliva "+ subtotaliva);

      Date c = Calendar.getInstance().getTime();
      SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());

      String fecha = df.format(c);
      df = new SimpleDateFormat("HHmmss", Locale.getDefault());
      String hora = df.format(c);

      WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
      String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());

      jsoncliente.put("NombreUsuario",!cliente.isNull("Nombre1") ? cliente.getString("Nombre1"):"");
      jsoncliente.put("SegNombreUsuario","");
      jsoncliente.put("ApellidoUsuario",!cliente.isNull("Nombre1") ? cliente.getString("Apellido1"):"");
      jsoncliente.put("DireccionIP",ip);
      jsoncliente.put("IdUsuario",!cliente.isNull("UserId") ? cliente.getString("UserId"):"");
      jsoncliente.put("MailUsuario",!cliente.isNull("CorreoElectronico") ? cliente.getString("CorreoElectronico"):"");
      jsoncliente.put("TipoIdUsuario","0"+(!cliente.isNull("IdTipoIdentificacion") ? cliente.getString("IdTipoIdentificacion"):""));
      jsoncliente.put("NumDocUsuario",!cliente.isNull("IdCliente") ? cliente.getString("IdCliente"):"");
      jsoncliente.put("TelefonoCliente","");
      jsoncliente.put("DireccionEntrega",!cliente.isNull("IdDireccion") ? cliente.getString("IdDireccion"):"");
      jsoncliente.put("DireccionCliente",!cliente.isNull("DireccionDomicilio") ? cliente.getString("DireccionDomicilio"):"");
      jsoncliente.put("PaisEntrega","EC");
      jsoncliente.put("PaisCompra","EC");
      Double envio=new Double(jObj.getString("ValorTransporteAdicional"));
      Double SubtotalIva0=new Double(0);
      Double SubtotalIva12=new Double(0);
      for (int i=0; i<items.length(); i++){
        JSONObject item = items.getJSONObject(i);
        jsonitem = new JSONObject();
        BigDecimal bd0 = new BigDecimal(!item.isNull("CostoConImpuesto") ? (Double.parseDouble(item.getString("CostoConImpuesto"))*100):0.00);
        bd0 = bd0.setScale(2, RoundingMode.HALF_UP);
        jsonitem.put("Nombre",!item.isNull("Sku") ? item.getString("Sku"):"");
        jsonitem.put("Descripcion","");
        jsonitem.put("Precio",bd0);
        jsonitem.put("Cantidad",!item.isNull("Cantidad") ? item.getString("Cantidad"):"");
        if(item.getInt("IdImpuestoIvaVenta")==1){//tiene iva
          SubtotalIva12+=(Double.parseDouble(item.getString("CostoConImpuesto")))*Double.parseDouble(item.getString("Cantidad"));
        }else{
          SubtotalIva0+=(Double.parseDouble(item.getString("CostoConImpuesto")))*Double.parseDouble(item.getString("Cantidad"));
        }
        jsonitems.put(jsonitem);
      }

      SubtotalIva0+=envio;

      BigDecimal bd1 = new BigDecimal(SubtotalIva0);
      bd1 = bd1.setScale(2, RoundingMode.HALF_UP);
      BigDecimal bd2 = new BigDecimal(SubtotalIva12);
      bd2 = bd2.setScale(2, RoundingMode.HALF_UP);

      jsonproducto.put("Item",jsonitems);
      json.put("IdTransaccion",hora);
      json.put("IdComercio",IDCOMERCIO);
      json.put("Fecha",fecha);
      json.put("Hora",hora);
      json.put("MontoTotal",Double.parseDouble(TOTAL)*100);//Double.parseDouble(TOTAL)*100);//decimales
      json.put("SubtotalIva0",bd1.doubleValue()*100);
      json.put("SubtotalIva12",bd2.doubleValue()*100);
      json.put("MontoIce","000");
      json.put("MontoIva","000");
      json.put("CodigoVerificacion","");//no va
      json.put("DatosCliente",jsoncliente);
      json.put("DatosProducto",jsonproducto);

      System.out.println("items "+ json.toString());
      return json.toString();

    }catch (Exception ex){
      System.out.println(ex.getMessage());
    }
    return json.toString();
  }

  private static String getTwoDecimals(double value){
    DecimalFormat df = new DecimalFormat("0.00");
    return df.format(value);
  }

  public void pagar(String json, String IdTransaccion){

    try {

      this.IdTransaccion=IdTransaccion;


      Handler handler = new Handler(Looper.getMainLooper());
      handler.post(new Runnable() {
        @Override
        public void run() {
          // Instantiate a FlutterEngine.
          flutterEngine = new FlutterEngine(MainActivity.this);
          // Configure an initial route.
          flutterEngine.getNavigationChannel().setInitialRoute(getdata(json));
          // Start executing Dart code to pre-warm the FlutterEngine.
          flutterEngine.getDartExecutor().executeDartEntrypoint(
            DartExecutor.DartEntrypoint.createDefault()
          );

          // Cache the FlutterEngine to be used by FlutterActivity or FlutterFragment.
          FlutterEngineCache
            .getInstance()
            .put(IdTransaccion, flutterEngine);

          startActivityForResult(
            FlutterActivity
              .withCachedEngine(IdTransaccion)
              .build(MainActivity.this),999
          );
        }
      });



    }catch (Exception ex){
      System.out.println("ERROR :");
      System.out.println(ex.getMessage());
    }
  }

  public JavaScriptHandler getJavaScriptHandler() {
    return new JavaScriptHandler(this.getApplicationContext());
  }

  @Override
  public void _pagosDatos(String _datospago) {

  }

  @Override
  public void _pagosMensaje(String titulo, String mensaje) {

  }

  @Override
  public void _pagosMensajeNoCerrar(String titulo, String mensaje, int _posicion) {

  }

  @Override
  public void _pagosValidacion(int _posicion) {

  }

  @Override
  public void _pagosError(int error) {

  }

  @Override
  public void _pagosDetalle(int position, boolean isChecked, boolean _validar) {

  }

  @Override
  public void _pagosMenu(String tipo, String opcion) {

  }

  public class JavaScriptHandler {
    CordovaActivity parentActivity;
    private Context mContext;

    public JavaScriptHandler(final Context context) {
      this.mContext = context;
    }

    @JavascriptInterface
    public void mNativeFunction(String json, String IdTransaccion) {
      System.out.println("Hello from ionic");
      System.out.println(json);
      System.out.println("IdTransaccion");
      System.out.println(IdTransaccion);
      pagar(json, IdTransaccion);
    }
  }
 /* @Override
  protected void onResume() {
    System.out.println("Hello from onResume");
    super.onResume();
    String userId = "No data received22";
    Intent intent = getIntent();
    if (intent != null && intent.getAction() != null && intent.getAction().equals("android.intent.action.MAIN")){
      System.out.println("Hello from Flutter");
      Bundle bundle = intent.getBundleExtra("data");
      if (bundle != null) {
        userId = bundle.getString("user_id");
        Log.d(TAG,"user_id22....:"+userId);
        userId = " User id is $userId";
      }
    }
    Log.d(TAG,"user_id2....:"+userId);
  }*/

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    Log.d(TAG,"onActivityResult...requestcode:"+requestCode+" resultcode:"+resultCode);
    super.onActivityResult(requestCode, resultCode, data);//10, 13 cuando viene de solicitud
    if(requestCode==999){


      try {
        HashMap status = new HashMap();
        status.put("authorized",true);

//        JSONObject obj = new JSONObject(status);

//        appView.sendPluginResult(PluginResult.Status.OK,obj);
//        appView.loadUrl("javascript:ChangeColor()");

//        PluginResult dataResult = new PluginResult(PluginResult.Status.OK, obj);
//        dataResult.setKeepCallback(true);
//        appView.handleResume(true);
//        webView.setFocusable(true);
//        webView.goBack();
        /*webView.evaluateJavascript("javascript:ChangeColor();", respuesta -> {
          try{
            System.out.println("respuesta");
            System.out.println(respuesta);
          }catch (Exception ex){
            System.out.println("Exception");
            System.out.println(ex);
          }
        });*/
          //.sendPluginResult(dataResult);

//        webView.loadUrl("javascript:ChangeColor()");

        /*SpotsDialog d = Alerta.mostrarProgreso(this);
        d.setCancelable(true);
        d.show()*/;

//        List<String> datos = url.getPagosPOST(cash, ctaDebito, gestorPagos, facilito, edttitular.getText().toString(), TIPOTRANSACCION, codigo,
//          Formato.numeroToPago(Double.valueOf(totalPago.getText().toString().replace(",", "")) * 100), String.valueOf(comisionAPagar), descripcion, Formato.urlEncodeField(nombreempresa), Formato.urlEncodeField(strcorreo), Formato.urlEncodeField(descripcion_Retorno),"");
//        new PagosHilo(this, this, d, 6, 0).execute("URL", "datos");
      } catch (Exception e) {
        e.printStackTrace();
      }

      /*dialog = new ProgressDialog(MainActivity.this);
      dialog.setMessage("Doing something, please wait.");
      dialog.show();*/
//      flutterEngine.destroy();
      System.out.println("IdTransaccion");
      System.out.println(this.IdTransaccion);
      FlutterEngineCache.getInstance().remove(this.IdTransaccion);
      try{
//        FlutterEngineCache.getInstance().get(this.IdTransaccion).destroy();
      }catch (Exception e){
        System.out.println(e);
      }
    }
  }

}
