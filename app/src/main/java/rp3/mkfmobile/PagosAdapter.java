package rp3.mkfmobile;

/**
 * Created by aalvarado on 9/01/17.
 */

public interface PagosAdapter {
    void _pagosDatos(String _datospago);

    void _pagosMensaje(String titulo, String mensaje);

    void _pagosMensajeNoCerrar(String titulo, String mensaje, int _posicion);

    void _pagosValidacion(int _posicion);

    void _pagosError(int error);

    void _pagosDetalle(int position, boolean isChecked, boolean _validar);

    void _pagosMenu(String tipo, String opcion);

}
