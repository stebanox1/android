package rp3.mkfmobile;

import android.content.Context;
import android.os.AsyncTask;

import dmax.dialog.SpotsDialog;

/**
 * Created by gandrade on 13/12/16.
 */
public class PagosHilo extends AsyncTask<String, Integer, Boolean> {

    private Context context;
    private PagosAdapter adapter;
    private SpotsDialog dialog;
    private Integer opcion;
    private Integer posicion;
    private RespMovil respPago;

    public PagosHilo(Context con, PagosAdapter adapter, SpotsDialog dialog, Integer opcion, Integer _posicion) {
        this.context = con;
        this.adapter = adapter;
        this.dialog = dialog;
        this.opcion = opcion;
        this.posicion = _posicion;
    }


    @Override
    protected Boolean doInBackground(String... url) {
        try {
            if (!_doInBackgroundD(url))
                    return false;

        } catch (Exception ex) {
            return false;
        }
        return true;
    }


    protected Boolean _doInBackgroundD(String... url) {
        try {
            if(url.length>1 && opcion==6) {
                respPago = new Webservice(context).postObjeto(RespMovil.class, url[0], url[1], false);
                if (respPago == null) {
                    return false;
                }
            }else{
                return true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        if (result) {
            switch (opcion) {
                case 6:
                    resultadoPago();
                    break;
                default:
                    break;
            }
        } else {
            adapter._pagosError(113);
        }
    }

    private void resultadoPago() {
        if (respPago.isSesion()) {
            String res = (respPago.getError() != null) ? respPago.getError() : respPago.getError();
            if (res.equalsIgnoreCase("0") || res.equalsIgnoreCase("2")) {
//                adapter._pagosRespPago(respPago);
            } else {
                adapter._pagosMensaje(respPago.getTitulo(), respPago.getMensaje());
            }
        } else {
            adapter._pagosError(2);
        }
    }

}

