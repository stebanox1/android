package rp3.mkfmobile;

import java.io.Serializable;

/**
 * Pojo para datos
 */
public class RespMovil implements Serializable {

    private static final long serialVersionUID = 1L;
    private String error;
    private String titulo;
    private String tasa;
    private String interes;
    private Boolean valido;
    private String impuesto;
    private String mensaje;
    private boolean sesion;
    private boolean registrado;
    private boolean estadoUsuario;
    private String codmensaje;
    private String icono;

    public RespMovil() {
        super();
        titulo="";
        mensaje="";
    }

    public String getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(String impuesto) {
        this.impuesto = impuesto;
    }

    public String getInteres() {
        return interes;
    }

    public void setInteres(String interes) {
        this.interes = interes;
    }

    public String getTasa() {
        return tasa;
    }

    public void setTasa(String tasa) {
        this.tasa = tasa;
    }

    public String getMensaje() {
        return mensaje;
    }


    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public boolean isSesion() {
        return sesion;
    }

    public void setSesion(boolean sesion) {
        this.sesion = sesion;
    }

    public Boolean isValido() {
        return valido;
    }

    public void setValido(Boolean valido) {
        this.valido = valido;
    }

    public boolean isRegistrado() {
        return registrado;
    }

    public void setRegistrado(boolean registrado) {
        this.registrado = registrado;
    }

    public boolean isEstadoUsuario() {
        return estadoUsuario;
    }

    public void setEstadoUsuario(boolean estadoUsuario) {
        this.estadoUsuario = estadoUsuario;
    }

    public String getCodmensaje() {
        return codmensaje;
    }

    public void setCodmensaje(String codmensaje) {
        this.codmensaje = codmensaje;
    }

    public String getIcono() {
        return icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }
}
