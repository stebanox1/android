package rp3.mkfmobile;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.zip.GZIPInputStream;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;

/**
 * Created by eandrade on 15/09/20.
 */
public class Webservice {
    private int timeout = 45000;
    private String data = "";
    private Context context;

    public Webservice(Context con) {
        this.context = con;
    }

    public <T> List<T> consultarLista(Class<T[]> token, String strUrl) throws Exception {
        if (strUrl != null && !strUrl.isEmpty()) {
            strUrl = strUrl.replace(" ", "%20");
            data = "";
            data = getDatos(strUrl);
            if (!data.equals("")) {
                T[] lista = new Gson().fromJson(data, token);
                return Arrays.asList(lista);
            }
        }
        return null;
    }

    public <T> T consultarObjeto(Class<T> token, String strUrl) throws Exception {
        if (strUrl != null && !strUrl.isEmpty()) {
            strUrl = strUrl.replace(" ", "%20");
            data = "";
            data = getDatos(strUrl);
            if (!data.equals("")) {
                T lista = new Gson().fromJson(data, token);
                return lista;
            }
        }
        return null;
    }

    public <T> T postObjeto(Class<T> token, String strUrl, String dataPhoto, boolean _replace) throws Exception {
        strUrl = strUrl.replace(" ", "%20");
        if (_replace) {
            dataPhoto = dataPhoto.replace(" ", "%20");
        }
        data = "";
        HttpsURLConnection urlConnection = null;
        HttpURLConnection http = null;

        try {
            URL url = new URL(strUrl);
            Log.d("URL",url+"");
            //Abro la conexion
            if (url.getProtocol().toLowerCase().equals("https")) {
                // Agrego el contexto
//                SSLSocketFactory sslContext = new GetSSLContex().getContext(context);
                urlConnection = (HttpsURLConnection) url.openConnection();
//                urlConnection.setSSLSocketFactory(sslContext);
                urlConnection.setHostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname, SSLSession session) {
                        return true;
                    }
                });
                http = urlConnection;
            } else {
                http = (HttpURLConnection) url.openConnection();
            }
            http.setDoInput(true);
            http.setDoOutput(true);
            http.setUseCaches(false);
            http.setChunkedStreamingMode(1024);
            http.setConnectTimeout(timeout);
            http.setReadTimeout(timeout);
            http.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
//            http.addRequestProperty("Content-Type", "application/json; charset=UTF-8");
            http.addRequestProperty("Content-Type", "application/json;application/x-www-form-urlencoded; charset=UTF-8");
            http.addRequestProperty("Accept", "application/json");
            http.addRequestProperty("Accept-Encoding", "gzip");
            http.setRequestMethod("POST");
            http.connect();
            //Write
            OutputStream outputStream = http.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            writer.write(dataPhoto);
            writer.close();
            outputStream.close();
            //Response
            if (http.getResponseCode() == HttpURLConnection.HTTP_OK) {
              BufferedReader in = new BufferedReader(
                new InputStreamReader(getDataResponse(http)));
              String inputLine;
              StringBuffer response = new StringBuffer();

              while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
              }

              in.close();
              data=response.toString();
//                InputStream inputStream = getDataResponse(http);
//                data = IOUtils.toString(inputStream, "UTF-8");
            } else {
                data = "";
            }
            if (!data.equals("")) {
                T lista = new Gson().fromJson(data, token);
                return lista;
            }
            return null;

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (urlConnection != null)
                    urlConnection.disconnect();
                if (http != null)
                    http.disconnect();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    private String getDatos(String strUrl) {

        HttpsURLConnection urlConnection = null;
        HttpURLConnection http = null;
        try {
            URL url = new URL(strUrl);
            Log.d("URL",url+"");
            //Abro la conexion
            if (url.getProtocol().toLowerCase().equals("https")) {
                // Agrego el contexto
//                SSLSocketFactory socket = new GetSSLContex().getContext(context);
                urlConnection = (HttpsURLConnection) url.openConnection();
//                urlConnection.setSSLSocketFactory(socket);
                urlConnection.setHostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname, SSLSession session) {
                        return true;
                    }
                });

                http = urlConnection;
            } else {
                http = (HttpURLConnection) url.openConnection();
            }

            //Aplico los cambios necesarios para la conexion
            http.setConnectTimeout(timeout);
            http.setReadTimeout(timeout);
            http.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
            http.addRequestProperty("Content-Type", "application/json; charset=UTF-8");
            http.addRequestProperty("Accept", "application/json");
            http.addRequestProperty("Accept-Encoding", "gzip");

            if (http.getResponseCode() == HttpURLConnection.HTTP_OK) {
              BufferedReader in = new BufferedReader(
                new InputStreamReader(getDataResponse(http)));
              String inputLine;
              StringBuffer response = new StringBuffer();

              while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
              }

              data=response.toString();
              in.close();
//                InputStream inputStream = getDataResponse(http);
//                data = IOUtils.toString(inputStream, "UTF-8");
            } else {
                data = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (urlConnection != null)
                    urlConnection.disconnect();
                if (http != null)
                    http.disconnect();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return data;
    }

    private InputStream getDataResponse(HttpURLConnection http) throws Exception {
        InputStream inputStream;
        String header = http.getContentEncoding();
        if (header != null && header.equalsIgnoreCase("gzip")) {
            inputStream = new GZIPInputStream(http.getInputStream());
        } else {
            inputStream = http.getInputStream();
        }
        return inputStream;
    }
}
